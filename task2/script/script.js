
// function to import the user-data into the index.html
$(function()
{

    //creating array for data
    var data = [];

    //get the data from url and run a foreach-loop
    $.getJSON('https://jsonplaceholder.typicode.com/users', function(data)
    {

        //start foreach-loop as long as there are data-records
        $.each(data, function(i, f)
        {

            // create a variable tblRow and write the information of the data-record in it
            var tblRow = "<tr>" +
                "<td><a href='http://www."+ f.website +"'><span class='glyphicon glyphicon-info-sign'></span></a></td>" +
                "<td>" + f.username + "</td>" +
                "<td><a href='mailto:" + f.email + "'>" + f.email + "</a></td>" +
                "<td><a href='http://maps.google.com/?q=" + f.address.geo.lat +", " + f.address.geo.lng + "'>" + f.address.street + " " + f.address.suite + "</a></td>" +
                "<td>" + f.address.city + "</td>" +
                "<td>" + f.company.name + "</td>" +
                "<td>" + f.company.catchPhrase + "</td>" +
                "</tr>";

            //append the record to the datatable
            $(tblRow).appendTo("#datatable");
        });
    });
});

//  ##########################
//  ##### SORT - FUNCTION ####
//  ##########################

// manipulate the css-class of "username" on click
$(".sort").click(function ()
{
    // check if class is a-z
    if($(this).hasClass("glyphicon glyphicon-sort-by-alphabet-alt"))
    {
        // change class to z-a
        $(".sort").removeClass("glyphicon glyphicon-sort-by-alphabet-alt").addClass("glyphicon glyphicon-sort-by-alphabet");
    }
    // check if class is z-a
    else if ($(this).hasClass("glyphicon glyphicon-sort-by-alphabet"))
    {
        // change class to a-z
        $(".sort").removeClass("glyphicon glyphicon-sort-by-alphabet").addClass("glyphicon glyphicon-sort-by-alphabet-alt");
    }
});


// sort datatable source: w3schools.com
function sortTable(n)
{
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("datatable");
    switching = true;

    //Set the sorting direction to ascending:
    dir = "asc";

    /*Make a loop that will continue until
     no switching has been done:*/
    while (switching)
    {

        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");

        /*Loop through all table rows (except the
         first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++)
        {

            //start by saying there should be no switching:
            shouldSwitch = false;

            /*Get the two elements you want to compare,
             one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            /*check if the two rows should switch place,
             based on the direction, asc or desc:*/
            if (dir == "asc")
            {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase())
                {

                    //if so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc")
            {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase())
                {

                    //if so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch)
        {

            /*If a switch has been marked, make the switch
             and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;

            //Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else
            {

            /*If no switching has been done AND the direction is "asc",
             set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc")
            {
                dir = "desc";
                switching = true;
            }
        }
    }
}

